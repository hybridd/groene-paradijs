<?php
namespace Afterpay\Payment\Model\Method\DigitalInvoiceNL;

/**
 * Interceptor class for @see \Afterpay\Payment\Model\Method\DigitalInvoiceNL
 */
class Interceptor extends \Afterpay\Payment\Model\Method\DigitalInvoiceNL implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Afterpay\Payment\Helper\Debug\Data $debugHelper, \Magento\Framework\Model\Context $context, \Magento\Framework\Registry $registry, \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory, \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory, \Magento\Payment\Helper\Data $paymentData, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Payment\Model\Method\Logger $logger, \Magento\Developer\Helper\Data $devHelper, \Afterpay\Payment\Helper\Service\Data $serviceHelper, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Backend\Model\Session\Quote $backendCheckoutSession, \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null, \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null, array $data = array())
    {
        $this->___init();
        parent::__construct($debugHelper, $context, $registry, $extensionFactory, $customAttributeFactory, $paymentData, $scopeConfig, $logger, $devHelper, $serviceHelper, $checkoutSession, $backendCheckoutSession, $resource, $resourceCollection, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function denyPayment(\Magento\Payment\Model\InfoInterface $payment)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'denyPayment');
        if (!$pluginInfo) {
            return parent::denyPayment($payment);
        } else {
            return $this->___callPlugins('denyPayment', func_get_args(), $pluginInfo);
        }
    }
}
