<?php
namespace Afterpay\Payment\Controller\Notify\Push;

/**
 * Interceptor class for @see \Afterpay\Payment\Controller\Notify\Push
 */
class Interceptor extends \Afterpay\Payment\Controller\Notify\Push implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Sales\Api\OrderRepositoryInterface $orderRepositoryInterface, \Afterpay\Payment\Helper\Service\Data $serviceHelper)
    {
        $this->___init();
        parent::__construct($context, $orderRepositoryInterface, $serviceHelper);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
