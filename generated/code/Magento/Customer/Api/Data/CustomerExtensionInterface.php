<?php
namespace Magento\Customer\Api\Data;

/**
 * ExtensionInterface class for @see \Magento\Customer\Api\Data\CustomerInterface
 */
interface CustomerExtensionInterface extends \Magento\Framework\Api\ExtensionAttributesInterface
{
    /**
     * @return boolean|null
     */
    public function getIsSubscribed();

    /**
     * @param boolean $isSubscribed
     * @return $this
     */
    public function setIsSubscribed($isSubscribed);

    /**
     * @return int|null
     */
    public function getPriceList();

    /**
     * @param int $priceList
     * @return $this
     */
    public function setPriceList($priceList);

    /**
     * @return string|null
     */
    public function getVatClass();

    /**
     * @param string $vatClass
     * @return $this
     */
    public function setVatClass($vatClass);
}
