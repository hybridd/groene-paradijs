<?php
namespace Magento\Customer\Api\Data;

/**
 * Extension class for @see \Magento\Customer\Api\Data\CustomerInterface
 */
class CustomerExtension extends \Magento\Framework\Api\AbstractSimpleObject implements CustomerExtensionInterface
{
    /**
     * @return boolean|null
     */
    public function getIsSubscribed()
    {
        return $this->_get('is_subscribed');
    }

    /**
     * @param boolean $isSubscribed
     * @return $this
     */
    public function setIsSubscribed($isSubscribed)
    {
        $this->setData('is_subscribed', $isSubscribed);
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPriceList()
    {
        return $this->_get('price_list');
    }

    /**
     * @param int $priceList
     * @return $this
     */
    public function setPriceList($priceList)
    {
        $this->setData('price_list', $priceList);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getVatClass()
    {
        return $this->_get('vat_class');
    }

    /**
     * @param string $vatClass
     * @return $this
     */
    public function setVatClass($vatClass)
    {
        $this->setData('vat_class', $vatClass);
        return $this;
    }
}
