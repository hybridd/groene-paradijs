<?php
namespace Magento\Quote\Model\Quote\Address\RateRequest;

/**
 * Interceptor class for @see \Magento\Quote\Model\Quote\Address\RateRequest
 */
class Interceptor extends \Magento\Quote\Model\Quote\Address\RateRequest implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(array $data = array())
    {
        $this->___init();
        parent::__construct($data);
    }

    /**
     * {@inheritdoc}
     */
    public function setData($key, $value = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setData');
        if (!$pluginInfo) {
            return parent::setData($key, $value);
        } else {
            return $this->___callPlugins('setData', func_get_args(), $pluginInfo);
        }
    }
}
