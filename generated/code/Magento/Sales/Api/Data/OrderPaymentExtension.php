<?php
namespace Magento\Sales\Api\Data;

/**
 * Extension class for @see \Magento\Sales\Api\Data\OrderPaymentInterface
 */
class OrderPaymentExtension extends \Magento\Framework\Api\AbstractSimpleObject implements OrderPaymentExtensionInterface
{
    /**
     * @return \Dealer4dealer\Xcore\Api\Data\PaymentCostInterface[]|null
     */
    public function getXcorePaymentCosts()
    {
        return $this->_get('xcore_payment_costs');
    }

    /**
     * @param \Dealer4dealer\Xcore\Api\Data\PaymentCostInterface[] $xcorePaymentCosts
     * @return $this
     */
    public function setXcorePaymentCosts($xcorePaymentCosts)
    {
        $this->setData('xcore_payment_costs', $xcorePaymentCosts);
        return $this;
    }

    /**
     * @return \Magento\Vault\Api\Data\PaymentTokenInterface|null
     */
    public function getVaultPaymentToken()
    {
        return $this->_get('vault_payment_token');
    }

    /**
     * @param \Magento\Vault\Api\Data\PaymentTokenInterface $vaultPaymentToken
     * @return $this
     */
    public function setVaultPaymentToken(\Magento\Vault\Api\Data\PaymentTokenInterface $vaultPaymentToken)
    {
        $this->setData('vault_payment_token', $vaultPaymentToken);
        return $this;
    }
}
