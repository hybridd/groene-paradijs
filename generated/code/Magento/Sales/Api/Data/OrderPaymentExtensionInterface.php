<?php
namespace Magento\Sales\Api\Data;

/**
 * ExtensionInterface class for @see \Magento\Sales\Api\Data\OrderPaymentInterface
 */
interface OrderPaymentExtensionInterface extends \Magento\Framework\Api\ExtensionAttributesInterface
{
    /**
     * @return \Dealer4dealer\Xcore\Api\Data\PaymentCostInterface[]|null
     */
    public function getXcorePaymentCosts();

    /**
     * @param \Dealer4dealer\Xcore\Api\Data\PaymentCostInterface[] $xcorePaymentCosts
     * @return $this
     */
    public function setXcorePaymentCosts($xcorePaymentCosts);

    /**
     * @return \Magento\Vault\Api\Data\PaymentTokenInterface|null
     */
    public function getVaultPaymentToken();

    /**
     * @param \Magento\Vault\Api\Data\PaymentTokenInterface $vaultPaymentToken
     * @return $this
     */
    public function setVaultPaymentToken(\Magento\Vault\Api\Data\PaymentTokenInterface $vaultPaymentToken);
}
