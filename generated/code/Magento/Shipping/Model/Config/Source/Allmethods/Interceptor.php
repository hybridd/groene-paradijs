<?php
namespace Magento\Shipping\Model\Config\Source\Allmethods;

/**
 * Interceptor class for @see \Magento\Shipping\Model\Config\Source\Allmethods
 */
class Interceptor extends \Magento\Shipping\Model\Config\Source\Allmethods implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Shipping\Model\Config $shippingConfig)
    {
        $this->___init();
        parent::__construct($scopeConfig, $shippingConfig);
    }

    /**
     * {@inheritdoc}
     */
    public function toOptionArray($isActiveOnlyFlag = false)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'toOptionArray');
        if (!$pluginInfo) {
            return parent::toOptionArray($isActiveOnlyFlag);
        } else {
            return $this->___callPlugins('toOptionArray', func_get_args(), $pluginInfo);
        }
    }
}
