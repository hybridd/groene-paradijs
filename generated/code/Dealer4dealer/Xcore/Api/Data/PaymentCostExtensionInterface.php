<?php
namespace Dealer4dealer\Xcore\Api\Data;

/**
 * ExtensionInterface class for @see \Dealer4dealer\Xcore\Api\Data\PaymentCostInterface
 */
interface PaymentCostExtensionInterface extends \Magento\Framework\Api\ExtensionAttributesInterface
{
}
