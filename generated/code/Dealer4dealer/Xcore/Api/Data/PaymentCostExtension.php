<?php
namespace Dealer4dealer\Xcore\Api\Data;

/**
 * Extension class for @see \Dealer4dealer\Xcore\Api\Data\PaymentCostInterface
 */
class PaymentCostExtension extends \Magento\Framework\Api\AbstractSimpleObject implements PaymentCostExtensionInterface
{
}
