<?php
namespace Magmodules\GoogleShopping\Model\Feed;

/**
 * Proxy class for @see \Magmodules\GoogleShopping\Model\Feed
 */
class Proxy extends \Magmodules\GoogleShopping\Model\Feed implements \Magento\Framework\ObjectManager\NoninterceptableInterface
{
    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager = null;

    /**
     * Proxied instance name
     *
     * @var string
     */
    protected $_instanceName = null;

    /**
     * Proxied instance
     *
     * @var \Magmodules\GoogleShopping\Model\Feed
     */
    protected $_subject = null;

    /**
     * Instance shareability flag
     *
     * @var bool
     */
    protected $_isShared = null;

    /**
     * Proxy constructor
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param string $instanceName
     * @param bool $shared
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager, $instanceName = '\\Magmodules\\GoogleShopping\\Model\\Feed', $shared = true)
    {
        $this->_objectManager = $objectManager;
        $this->_instanceName = $instanceName;
        $this->_isShared = $shared;
    }

    /**
     * @return array
     */
    public function __sleep()
    {
        return ['_subject', '_isShared', '_instanceName'];
    }

    /**
     * Retrieve ObjectManager from global scope
     */
    public function __wakeup()
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    }

    /**
     * Clone proxied instance
     */
    public function __clone()
    {
        $this->_subject = clone $this->_getSubject();
    }

    /**
     * Get proxied instance
     *
     * @return \Magmodules\GoogleShopping\Model\Feed
     */
    protected function _getSubject()
    {
        if (!$this->_subject) {
            $this->_subject = true === $this->_isShared
                ? $this->_objectManager->get($this->_instanceName)
                : $this->_objectManager->create($this->_instanceName);
        }
        return $this->_subject;
    }

    /**
     * {@inheritdoc}
     */
    public function generateAll()
    {
        return $this->_getSubject()->generateAll();
    }

    /**
     * {@inheritdoc}
     */
    public function generateByStore($storeId, $type = 'manual', $productIds = array(), $page = 1, $data = false)
    {
        return $this->_getSubject()->generateByStore($storeId, $type, $productIds, $page, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function getFeedData($products, $parents, $config, $parentRelations, $data = false)
    {
        return $this->_getSubject()->getFeedData($products, $parents, $config, $parentRelations, $data);
    }
}
