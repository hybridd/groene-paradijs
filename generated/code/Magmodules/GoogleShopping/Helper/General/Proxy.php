<?php
namespace Magmodules\GoogleShopping\Helper\General;

/**
 * Proxy class for @see \Magmodules\GoogleShopping\Helper\General
 */
class Proxy extends \Magmodules\GoogleShopping\Helper\General implements \Magento\Framework\ObjectManager\NoninterceptableInterface
{
    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager = null;

    /**
     * Proxied instance name
     *
     * @var string
     */
    protected $_instanceName = null;

    /**
     * Proxied instance
     *
     * @var \Magmodules\GoogleShopping\Helper\General
     */
    protected $_subject = null;

    /**
     * Instance shareability flag
     *
     * @var bool
     */
    protected $_isShared = null;

    /**
     * Proxy constructor
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param string $instanceName
     * @param bool $shared
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager, $instanceName = '\\Magmodules\\GoogleShopping\\Helper\\General', $shared = true)
    {
        $this->_objectManager = $objectManager;
        $this->_instanceName = $instanceName;
        $this->_isShared = $shared;
    }

    /**
     * @return array
     */
    public function __sleep()
    {
        return ['_subject', '_isShared', '_instanceName'];
    }

    /**
     * Retrieve ObjectManager from global scope
     */
    public function __wakeup()
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    }

    /**
     * Clone proxied instance
     */
    public function __clone()
    {
        $this->_subject = clone $this->_getSubject();
    }

    /**
     * Get proxied instance
     *
     * @return \Magmodules\GoogleShopping\Helper\General
     */
    protected function _getSubject()
    {
        if (!$this->_subject) {
            $this->_subject = true === $this->_isShared
                ? $this->_objectManager->get($this->_instanceName)
                : $this->_objectManager->create($this->_instanceName);
        }
        return $this->_subject;
    }

    /**
     * {@inheritdoc}
     */
    public function getCronEnabled()
    {
        return $this->_getSubject()->getCronEnabled();
    }

    /**
     * {@inheritdoc}
     */
    public function getStoreValue($path, $storeId = null, $scope = null)
    {
        return $this->_getSubject()->getStoreValue($path, $storeId, $scope);
    }

    /**
     * {@inheritdoc}
     */
    public function getStoreValueArray($path, $storeId = null, $scope = null)
    {
        return $this->_getSubject()->getStoreValueArray($path, $storeId, $scope);
    }

    /**
     * {@inheritdoc}
     */
    public function getUncachedStoreValue($path, $storeId)
    {
        return $this->_getSubject()->getUncachedStoreValue($path, $storeId);
    }

    /**
     * {@inheritdoc}
     */
    public function setConfigData($value, $key, $storeId = null)
    {
        return $this->_getSubject()->setConfigData($value, $key, $storeId);
    }

    /**
     * {@inheritdoc}
     */
    public function getExtensionVersion()
    {
        return $this->_getSubject()->getExtensionVersion();
    }

    /**
     * {@inheritdoc}
     */
    public function getMagentoVersion()
    {
        return $this->_getSubject()->getMagentoVersion();
    }

    /**
     * {@inheritdoc}
     */
    public function getEnabledArray($path)
    {
        return $this->_getSubject()->getEnabledArray($path);
    }

    /**
     * {@inheritdoc}
     */
    public function getEnabled()
    {
        return $this->_getSubject()->getEnabled();
    }

    /**
     * {@inheritdoc}
     */
    public function getGenerateEnabled($storeId = null)
    {
        return $this->_getSubject()->getGenerateEnabled($storeId);
    }

    /**
     * {@inheritdoc}
     */
    public function addTolog($id, $data)
    {
        return $this->_getSubject()->addTolog($id, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function getDateTime()
    {
        return $this->_getSubject()->getDateTime();
    }

    /**
     * {@inheritdoc}
     */
    public function getLocaleDate($storeId)
    {
        return $this->_getSubject()->getLocaleDate($storeId);
    }

    /**
     * {@inheritdoc}
     */
    public function isModuleOutputEnabled($moduleName = null)
    {
        return $this->_getSubject()->isModuleOutputEnabled($moduleName);
    }
}
