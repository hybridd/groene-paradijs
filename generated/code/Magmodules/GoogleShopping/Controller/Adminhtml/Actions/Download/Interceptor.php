<?php
namespace Magmodules\GoogleShopping\Controller\Adminhtml\Actions\Download;

/**
 * Interceptor class for @see \Magmodules\GoogleShopping\Controller\Adminhtml\Actions\Download
 */
class Interceptor extends \Magmodules\GoogleShopping\Controller\Adminhtml\Actions\Download implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\Controller\Result\RawFactory $resultRawFactory, \Magento\Framework\App\Response\Http\FileFactory $fileFactory, \Magmodules\GoogleShopping\Helper\Feed $feedHelper, \Magmodules\GoogleShopping\Helper\General $generalHelper, \Magento\Framework\App\Filesystem\DirectoryList $directoryList)
    {
        $this->___init();
        parent::__construct($context, $resultRawFactory, $fileFactory, $feedHelper, $generalHelper, $directoryList);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
