<?php
namespace Magmodules\GoogleShopping\Controller\Adminhtml\Actions\Preview;

/**
 * Interceptor class for @see \Magmodules\GoogleShopping\Controller\Adminhtml\Actions\Preview
 */
class Interceptor extends \Magmodules\GoogleShopping\Controller\Adminhtml\Actions\Preview implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magmodules\GoogleShopping\Helper\General $generalHelper, \Magmodules\GoogleShopping\Model\Feed $feedModel)
    {
        $this->___init();
        parent::__construct($context, $generalHelper, $feedModel);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
