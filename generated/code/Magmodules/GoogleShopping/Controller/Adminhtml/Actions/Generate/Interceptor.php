<?php
namespace Magmodules\GoogleShopping\Controller\Adminhtml\Actions\Generate;

/**
 * Interceptor class for @see \Magmodules\GoogleShopping\Controller\Adminhtml\Actions\Generate
 */
class Interceptor extends \Magmodules\GoogleShopping\Controller\Adminhtml\Actions\Generate implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magmodules\GoogleShopping\Model\Feed $feedModel, \Magmodules\GoogleShopping\Helper\General $generalHelper)
    {
        $this->___init();
        parent::__construct($context, $feedModel, $generalHelper);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
