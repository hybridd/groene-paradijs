<?php
namespace Icepay\IcpCore\Controller\Checkout\Cancel;

/**
 * Interceptor class for @see \Icepay\IcpCore\Controller\Checkout\Cancel
 */
class Interceptor extends \Icepay\IcpCore\Controller\Checkout\Cancel implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Customer\Model\Session $customerSession, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Sales\Model\OrderFactory $orderFactory, \Icepay\IcpCore\Model\Checkout\Factory $checkoutFactory, \Magento\Framework\Session\Generic $icepaySession, \Magento\Framework\Url\Helper\Data $urlHelper, \Magento\Customer\Model\Url $customerUrl, \Magento\Checkout\Model\Cart $cart, \Magento\Checkout\Model\Type\Onepage $onepage, \Psr\Log\LoggerInterface $logger)
    {
        $this->___init();
        parent::__construct($context, $customerSession, $checkoutSession, $orderFactory, $checkoutFactory, $icepaySession, $urlHelper, $customerUrl, $cart, $onepage, $logger);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
