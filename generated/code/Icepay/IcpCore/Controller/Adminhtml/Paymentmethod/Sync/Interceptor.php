<?php
namespace Icepay\IcpCore\Controller\Adminhtml\Paymentmethod\Sync;

/**
 * Interceptor class for @see \Icepay\IcpCore\Controller\Adminhtml\Paymentmethod\Sync
 */
class Interceptor extends \Icepay\IcpCore\Controller\Adminhtml\Paymentmethod\Sync implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Store\Model\StoreManagerInterface $storeManager, \Icepay\IcpCore\Api\PaymentmethodRepositoryInterface $paymentmethodRepository, \Icepay\IcpCore\Api\Data\PaymentmethodInterfaceFactory $paymentmethodDataFactory, \Magento\Framework\Api\FilterBuilder $filterBuilder, \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder, \Icepay\IcpCore\Model\PaymentmethodFactory $paymentmethodFactory, \Icepay\IcpCore\Model\IssuerFactory $issuerFactory, \Magento\Framework\Encryption\EncryptorInterface $encryptor)
    {
        $this->___init();
        parent::__construct($context, $resultPageFactory, $resultForwardFactory, $scopeConfig, $storeManager, $paymentmethodRepository, $paymentmethodDataFactory, $filterBuilder, $searchCriteriaBuilder, $paymentmethodFactory, $issuerFactory, $encryptor);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
