<?php
namespace Icepay\IcpCore\Controller\Adminhtml\Paymentmethod\Save;

/**
 * Interceptor class for @see \Icepay\IcpCore\Controller\Adminhtml\Paymentmethod\Save
 */
class Interceptor extends \Icepay\IcpCore\Controller\Adminhtml\Paymentmethod\Save implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory, \Icepay\IcpCore\Model\PaymentmethodFactory $paymentmethodFactory, \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder, \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Store\Model\StoreManagerInterface $storeManager)
    {
        $this->___init();
        parent::__construct($context, $resultPageFactory, $resultForwardFactory, $paymentmethodFactory, $transportBuilder, $inlineTranslation, $scopeConfig, $storeManager);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
