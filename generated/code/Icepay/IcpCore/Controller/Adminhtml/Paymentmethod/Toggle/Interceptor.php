<?php
namespace Icepay\IcpCore\Controller\Adminhtml\Paymentmethod\Toggle;

/**
 * Interceptor class for @see \Icepay\IcpCore\Controller\Adminhtml\Paymentmethod\Toggle
 */
class Interceptor extends \Icepay\IcpCore\Controller\Adminhtml\Paymentmethod\Toggle implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory, \Icepay\IcpCore\Model\PaymentmethodFactory $paymentmethodFactory)
    {
        $this->___init();
        parent::__construct($context, $resultPageFactory, $resultForwardFactory, $paymentmethodFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
