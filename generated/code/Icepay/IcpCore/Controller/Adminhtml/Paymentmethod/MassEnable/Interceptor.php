<?php
namespace Icepay\IcpCore\Controller\Adminhtml\Paymentmethod\MassEnable;

/**
 * Interceptor class for @see \Icepay\IcpCore\Controller\Adminhtml\Paymentmethod\MassEnable
 */
class Interceptor extends \Icepay\IcpCore\Controller\Adminhtml\Paymentmethod\MassEnable implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory, \Magento\Ui\Component\MassAction\Filter $filter, \Icepay\IcpCore\Model\ResourceModel\Paymentmethod\CollectionFactory $collectionFactory)
    {
        $this->___init();
        parent::__construct($context, $resultPageFactory, $resultForwardFactory, $filter, $collectionFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
