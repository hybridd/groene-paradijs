<?php
namespace Icepay\IcpCore\Model\PaymentMethod\IcepayAbstractMethod;

/**
 * Interceptor class for @see \Icepay\IcpCore\Model\PaymentMethod\IcepayAbstractMethod
 */
class Interceptor extends \Icepay\IcpCore\Model\PaymentMethod\IcepayAbstractMethod implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\Model\Context $context, \Magento\Framework\Registry $registry, \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory, \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory, \Magento\Payment\Helper\Data $paymentData, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Payment\Model\Method\Logger $logger, \Icepay\IcpCore\Api\PaymentmethodRepositoryInterface $paymentmethodRepository, \Magento\Framework\Api\FilterBuilder $filterBuilder, \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder, \Magento\Payment\Model\Checks\CanUseForCountry\CountryProvider $countryProvider, \Magento\Framework\Module\ModuleListInterface $moduleList, \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate, \Magento\Framework\Model\ResourceModel\AbstractResource $resource, \Magento\Framework\Data\Collection\AbstractDb $resourceCollection, \Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface $transactionBuilder, \Magento\Sales\Model\Order\Payment\Transaction\ManagerInterface $transactionManager, array $data = array())
    {
        $this->___init();
        parent::__construct($context, $registry, $extensionFactory, $customAttributeFactory, $paymentData, $scopeConfig, $storeManager, $logger, $paymentmethodRepository, $filterBuilder, $searchCriteriaBuilder, $countryProvider, $moduleList, $localeDate, $resource, $resourceCollection, $transactionBuilder, $transactionManager, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function denyPayment(\Magento\Payment\Model\InfoInterface $payment)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'denyPayment');
        if (!$pluginInfo) {
            return parent::denyPayment($payment);
        } else {
            return $this->___callPlugins('denyPayment', func_get_args(), $pluginInfo);
        }
    }
}
