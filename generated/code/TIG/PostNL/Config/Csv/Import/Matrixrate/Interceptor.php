<?php
namespace TIG\PostNL\Config\Csv\Import\Matrixrate;

/**
 * Interceptor class for @see \TIG\PostNL\Config\Csv\Import\Matrixrate
 */
class Interceptor extends \TIG\PostNL\Config\Csv\Import\Matrixrate implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\Model\Context $context, \Magento\Framework\Registry $registry, \Magento\Framework\App\Config\ScopeConfigInterface $config, \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList, \Magento\Framework\Filesystem $filesystem, \TIG\PostNL\Service\Import\Matrixrate\Data $matrixrateData, \Magento\Framework\App\RequestInterface $request, \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null, \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null, array $data = array())
    {
        $this->___init();
        parent::__construct($context, $registry, $config, $cacheTypeList, $filesystem, $matrixrateData, $request, $resource, $resourceCollection, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'afterSave');
        if (!$pluginInfo) {
            return parent::afterSave();
        } else {
            return $this->___callPlugins('afterSave', func_get_args(), $pluginInfo);
        }
    }
}
