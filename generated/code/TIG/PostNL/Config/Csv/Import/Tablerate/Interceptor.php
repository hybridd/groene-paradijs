<?php
namespace TIG\PostNL\Config\Csv\Import\Tablerate;

/**
 * Interceptor class for @see \TIG\PostNL\Config\Csv\Import\Tablerate
 */
class Interceptor extends \TIG\PostNL\Config\Csv\Import\Tablerate implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\Model\Context $context, \Magento\Framework\Registry $registry, \Magento\Framework\App\Config\ScopeConfigInterface $config, \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList, \TIG\PostNL\Model\ResourceModel\TablerateFactory $tablerateFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \TIG\PostNL\Service\Import\Csv $csv, \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null, \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null, array $data = array())
    {
        $this->___init();
        parent::__construct($context, $registry, $config, $cacheTypeList, $tablerateFactory, $storeManager, $csv, $resource, $resourceCollection, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'afterSave');
        if (!$pluginInfo) {
            return parent::afterSave();
        } else {
            return $this->___callPlugins('afterSave', func_get_args(), $pluginInfo);
        }
    }
}
