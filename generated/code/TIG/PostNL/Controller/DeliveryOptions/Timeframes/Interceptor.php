<?php
namespace TIG\PostNL\Controller\DeliveryOptions\Timeframes;

/**
 * Interceptor class for @see \TIG\PostNL\Controller\DeliveryOptions\Timeframes
 */
class Interceptor extends \TIG\PostNL\Controller\DeliveryOptions\Timeframes implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \TIG\PostNL\Model\OrderFactory $orderFactory, \Magento\Checkout\Model\Session $checkoutSession, \TIG\PostNL\Service\Carrier\QuoteToRateRequest $quoteToRateRequest, \TIG\PostNL\Helper\AddressEnhancer $addressEnhancer, \TIG\PostNL\Webservices\Endpoints\DeliveryDate $deliveryDate, \TIG\PostNL\Webservices\Endpoints\TimeFrame $timeFrame, \TIG\PostNL\Service\Carrier\Price\Calculator $calculator, \TIG\PostNL\Config\CheckoutConfiguration\IsDeliverDaysActive $isDeliverDaysActive)
    {
        $this->___init();
        parent::__construct($context, $orderFactory, $checkoutSession, $quoteToRateRequest, $addressEnhancer, $deliveryDate, $timeFrame, $calculator, $isDeliverDaysActive);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
