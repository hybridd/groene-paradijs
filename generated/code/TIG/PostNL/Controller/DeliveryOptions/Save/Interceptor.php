<?php
namespace TIG\PostNL\Controller\DeliveryOptions\Save;

/**
 * Interceptor class for @see \TIG\PostNL\Controller\DeliveryOptions\Save
 */
class Interceptor extends \TIG\PostNL\Controller\DeliveryOptions\Save implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \TIG\PostNL\Model\OrderFactory $orderFactory, \TIG\PostNL\Model\OrderRepository $orderRepository, \TIG\PostNL\Service\Carrier\QuoteToRateRequest $quoteToRateRequest, \TIG\PostNL\Helper\DeliveryOptions\OrderParams $orderParams, \Magento\Checkout\Model\Session $checkoutSession, \TIG\PostNL\Helper\DeliveryOptions\PickupAddress $pickupAddress)
    {
        $this->___init();
        parent::__construct($context, $orderFactory, $orderRepository, $quoteToRateRequest, $orderParams, $checkoutSession, $pickupAddress);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
