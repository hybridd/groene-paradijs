<?php
namespace TIG\PostNL\Controller\Adminhtml\Carrier\Matrixrate\Export;

/**
 * Interceptor class for @see \TIG\PostNL\Controller\Adminhtml\Carrier\Matrixrate\Export
 */
class Interceptor extends \TIG\PostNL\Controller\Adminhtml\Carrier\Matrixrate\Export implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Framework\App\Response\Http\FileFactory $fileFactory, \TIG\PostNL\Service\Export\Csv\Matrixrate $export, \TIG\PostNL\Api\MatrixrateRepositoryInterface $matrixrateRepository)
    {
        $this->___init();
        parent::__construct($context, $storeManager, $fileFactory, $export, $matrixrateRepository);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
