<?php
namespace TIG\PostNL\Controller\Adminhtml\Shipment\ConfirmShipping;

/**
 * Interceptor class for @see \TIG\PostNL\Controller\Adminhtml\Shipment\ConfirmShipping
 */
class Interceptor extends \TIG\PostNL\Controller\Adminhtml\Shipment\ConfirmShipping implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Sales\Model\Order\ShipmentRepository $shipmentRepository, \TIG\PostNL\Helper\Tracking\Track $track, \TIG\PostNL\Webservices\Endpoints\Confirming $confirming, \TIG\PostNL\Api\ShipmentRepositoryInterface $repositoryInterface, \TIG\PostNL\Helper\Data $helper)
    {
        $this->___init();
        parent::__construct($context, $shipmentRepository, $track, $confirming, $repositoryInterface, $helper);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
