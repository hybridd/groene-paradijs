<?php
namespace TIG\PostNL\Controller\Adminhtml\Shipment\SaveMulticolli;

/**
 * Interceptor class for @see \TIG\PostNL\Controller\Adminhtml\Shipment\SaveMulticolli
 */
class Interceptor extends \TIG\PostNL\Controller\Adminhtml\Shipment\SaveMulticolli implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \TIG\PostNL\Api\ShipmentRepositoryInterface $shipmentRepository, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory)
    {
        $this->___init();
        parent::__construct($context, $shipmentRepository, $resultJsonFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
