<?php
namespace TIG\PostNL\Controller\Adminhtml\Shipment\ChangeConfirmation;

/**
 * Interceptor class for @see \TIG\PostNL\Controller\Adminhtml\Shipment\ChangeConfirmation
 */
class Interceptor extends \TIG\PostNL\Controller\Adminhtml\Shipment\ChangeConfirmation implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \TIG\PostNL\Service\Shipment\ShipmentService $shipmentService, \TIG\PostNL\Service\Shipment\Label\DeleteLabel $labelDeleteHandler, \TIG\PostNL\Service\Shipment\Barcode\DeleteBarcode $barcodeDeleteHandler, \TIG\PostNL\Service\Shipment\Track\DeleteTrack $trackDeleteHandler)
    {
        $this->___init();
        parent::__construct($context, $shipmentService, $labelDeleteHandler, $barcodeDeleteHandler, $trackDeleteHandler);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
