<?php
namespace TIG\PostNL\Controller\Adminhtml\Shipment\MassPrintShippingLabel;

/**
 * Interceptor class for @see \TIG\PostNL\Controller\Adminhtml\Shipment\MassPrintShippingLabel
 */
class Interceptor extends \TIG\PostNL\Controller\Adminhtml\Shipment\MassPrintShippingLabel implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Ui\Component\MassAction\Filter $filter, \Magento\Sales\Model\ResourceModel\Order\Shipment\CollectionFactory $collectionFactory, \TIG\PostNL\Service\Shipment\Labelling\GetLabels $getLabels, \TIG\PostNL\Controller\Adminhtml\PdfDownload $getPdf, \TIG\PostNL\Helper\Tracking\Track $track, \TIG\PostNL\Service\Handler\BarcodeHandler $barcodeHandler)
    {
        $this->___init();
        parent::__construct($context, $filter, $collectionFactory, $getLabels, $getPdf, $track, $barcodeHandler);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
