<?php
namespace TIG\PostNL\Controller\Adminhtml\Order\CreateShipments;

/**
 * Interceptor class for @see \TIG\PostNL\Controller\Adminhtml\Order\CreateShipments
 */
class Interceptor extends \TIG\PostNL\Controller\Adminhtml\Order\CreateShipments implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Ui\Component\MassAction\Filter $filter, \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory, \Magento\Sales\Model\Convert\Order $convertOrder)
    {
        $this->___init();
        parent::__construct($context, $filter, $collectionFactory, $convertOrder);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
